import React from "react";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { Icon } from "native-base";

import Colors from "../constants/Colors";
import SearchScreen from "../screens/SearchScreen/SearchScreen";
import StatisticsScreen from "../screens/StatisticsScreen";
import HistoryScreen from "../screens/HistoryScreen";
import FavoritesScreen from "../screens/FavoritesScreen/FavoritesScreen";
import { createStackNavigator } from "react-navigation-stack";
import RecipeDetailsScreen from "../screens/SearchScreen/RecipeDetailsScreen/RecipeDetailsScreen";
import FavoriteButton from "../screens/FavoritesScreen/FavoriteButton";


const SearchStack = createStackNavigator(
	{
		RecipeSearch: {
			screen: SearchScreen,
			navigationOptions: { headerShown: false },
		},
		RecipeDetails: {
			screen: RecipeDetailsScreen,
			navigationOptions: { headerRight: <FavoriteButton /> },
		},
	},
	{
		initialRouteName: "RecipeSearch",
	}
);

SearchStack.navigationOptions = ({ navigation }) => {
	let tabBarVisible = true;
	if (navigation.state.index > 0) {
		tabBarVisible = false;
	}
	return {
		tabBarVisible,
	};
};

const FavoritesStack = createStackNavigator(
	{
		FavoriteList: {
			screen: FavoritesScreen,
			navigationOptions: { headerShown: false },
		},
		FavoriteDetails: {
			screen: RecipeDetailsScreen,
			navigationOptions: { headerRight: <FavoriteButton /> },
		},
	},
	{ initialRouteName: "FavoriteList" }
);

FavoritesStack.navigationOptions = ({ navigation }) => {
	let tabBarVisible = true;
	if (navigation.state.index > 0) {
		tabBarVisible = false;
	}
	return {
		tabBarVisible,
	};
};

const AppNavigator = createBottomTabNavigator(
	{
		Search: SearchStack,
		Statistics: StatisticsScreen,
		History: HistoryScreen,
		Favorites: FavoritesStack,
	},
	{
		defaultNavigationOptions: ({ navigation }) => ({
			tabBarIcon: ({ focused, horizontal, tintColor }) => {
				const { routeName } = navigation.state;
				let iconName;
				if (routeName === "Search") {
					iconName = "search";
				} else if (routeName === "Statistics") {
					iconName = "sort";
				} else if (routeName === "History") {
					iconName = "access-time";
				} else if (routeName === "Favorites") {
					iconName = "favorite";
				}

				return <Icon type="MaterialIcons" name={iconName} size={25} style={{color: tintColor}} />;
			},
		}),
		tabBarOptions: {
			activeTintColor: Colors.tabIconSelected,
			inactiveTintColor: Colors.tabIconDefault,
		},
		initialRouteName: "Search",
	}
);

export default AppNavigator;
