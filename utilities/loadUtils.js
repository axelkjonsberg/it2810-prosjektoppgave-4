import { getWordcloud, getRecipe, searchRecipes } from "./queries";

export async function loadRecipes(searchCriteria, loadMode, existingList) {
	try {
		const result = await searchRecipes(searchCriteria);
		if (result.error) {
			throw result.error;
		}
		let newList;
		if (loadMode === "append") {
			newList = existingList.concat(result.data.recipes);
		} else if (loadMode === "load") {
			newList = result.data.recipes;
		}
		return newList;
	} catch (error) {
		throw error;
	}
}

export async function loadRecipe(id) {
	try {
		const result = await getRecipe(id);
		if (result.error) {
			throw result.error;
		}
		if (result.data.recipe) {
			return result.data.recipe;
		}
	} catch (error) {
		throw error;
	}
}

export async function loadWordFrequency() {
	try {
		const result = await getWordcloud();
		if (result.error) {
			throw result.error;
		} else {
			return result.data.wordcloud;
		}
	} catch (error) {
		throw error;
	}
}
