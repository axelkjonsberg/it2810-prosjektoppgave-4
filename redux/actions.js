import {
	SET_SORT_FIELD,
	TOGGLE_SORT_DESC,
	SET_RECIPE_SEARCH_VALUE,
	SET_RECIPE_LIMIT,
	SET_MAX_INGREDIENTS,
	SET_MAX_MINUTES,
	SET_MAX_STEPS,
	NEXT_RECIPE_PAGE,
	PREV_RECIPE_PAGE,
	SET_LOADING,
	RESET_PAGE,
	SET_LOAD_MODE_LOAD,
	SET_LOAD_MODE_APPEND,
	BEGIN_LOADING_WORD_FREQUENCY,
	FAILURE_LOADING_WORD_FREQUENCY,
	SUCCESS_LOADING_WORD_FREQUENCY,
	SET_HISTORY,
	SET_FAVORITES,
} from "./actionTypes";

export const setMaxIngredients = maxIngredients => ({
	type: SET_MAX_INGREDIENTS,
	payload: { maxIngredients },
});
export const setMaxSteps = maxSteps => ({
	type: SET_MAX_STEPS,
	payload: { maxSteps },
});
export const setMaxMinutes = maxMinutes => ({
	type: SET_MAX_MINUTES,
	payload: { maxMinutes },
});

export const setSearchValue = searchValue => ({
	type: SET_RECIPE_SEARCH_VALUE,
	payload: { searchValue },
});

export const setRecipeLimit = limit => ({
	type: SET_RECIPE_LIMIT,
	payload: { limit },
});

export const setSortField = sortField => ({
	type: SET_SORT_FIELD,
	payload: { sortField },
});
export const toggleDescList = () => ({
	type: TOGGLE_SORT_DESC,
});

export const nextRecipePage = () => ({
	type: NEXT_RECIPE_PAGE,
});
export const prevRecipePage = () => ({
	type: PREV_RECIPE_PAGE,
});
export const resetPage = () => ({
	type: RESET_PAGE,
});

export const setLoading = loading => ({
	type: SET_LOADING,
	payload: { loading },
});

export const setLoadModeLoad = () => ({
	type: SET_LOAD_MODE_LOAD,
	payload: { loadMode: "load" },
});
export const setLoadModeAppend = () => ({
	type: SET_LOAD_MODE_APPEND,
	payload: { loadMode: "append" },
});

export const beginLoadingWordFrequency = () => ({
	type: BEGIN_LOADING_WORD_FREQUENCY,
});
export const failureLoadingWordFrequency = error => ({
	type: FAILURE_LOADING_WORD_FREQUENCY,
	payload: { error },
});
export const successLoadingWordFrequency = wordFrequency => ({
	type: SUCCESS_LOADING_WORD_FREQUENCY,
	payload: { wordFrequency },
});

export const setHistory = history => ({
	type: SET_HISTORY,
	payload: { history },
});

export const setFavorites = favorites => ({
	type: SET_FAVORITES,
	payload: { favorites },
});
