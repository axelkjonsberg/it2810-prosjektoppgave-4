import React, { useState, useEffect } from "react";
import { StyleSheet } from "react-native";
import { View, Text, Container, Content, H3, H2, Icon } from "native-base";
import { connect } from "react-redux";
import * as Progress from "react-native-progress";

import { loadRecipe } from "../../../utilities/loadUtils";
import { setLoading } from "./../../../redux/actions";
import Colors from "../../../constants/Colors";
import { formatText, formatMinutes } from "../../../utilities/formatUtils";
import { capitalizeFirstLetter } from "./../../../utilities/formatUtils";

const RecipeDetailsScreen = props => {
	const [recipe, setRecipe] = useState();

	useEffect(() => {
		const recipeId = props.navigation.getParam("recipeId");
		if (recipeId) {
			props.dispatch(setLoading(true));
			loadRecipe(recipeId, setRecipe)
				.then(res => setRecipe(res))
				.catch(error => console.error(error))
				.finally(() => props.dispatch(setLoading(false)));
		}
	}, []);

	return (
		<Container>
			<Progress.Bar
				indeterminate
				color={props.loading ? Colors.primary : "white"}
				width={null}
				borderWidth={0}
				borderRadius={0}
			/>
			<Content padder>
				{recipe && (
					<View>
						<Text style={styles.title} selectable>
							{formatText(recipe.name)}
						</Text>
						<Text style={styles.prepTime} selectable>
							{recipe.minutes
								? formatMinutes(recipe.minutes) + " to prepare"
								: "Prep time not specified"}
						</Text>
						<Text style={styles.description} selectable>
							{formatText(recipe.description)}
						</Text>
						<Text style={styles.ingredientsHeader} selectable>
							Ingredients
						</Text>
						{recipe.ingredients.map((ingredient, index) => (
							<View key={index} style={styles.ingredientWrapper}>
								<Icon
									type='MaterialCommunityIcons'
									name='checkbox-blank-circle'
									style={{ fontSize: 8 }}
								/>
								<Text style={styles.ingredient} selectable>
									{capitalizeFirstLetter(ingredient)}
								</Text>
							</View>
						))}
						<Text style={styles.stepsHeader} selectable>
							Steps
						</Text>
						{recipe.steps.map((step, index) => (
							<View key={index}>
								<Text style={styles.step} selectable>{`${index +
									1}. ${formatText(step)}`}</Text>
							</View>
						))}
					</View>
				)}
			</Content>
		</Container>
	);
};

const mapStateToProps = state => ({
	loading: state.loading.loading,
});

export default connect(mapStateToProps)(RecipeDetailsScreen);

const styles = StyleSheet.create({
	title: {
		textTransform: "capitalize",
		fontFamily: "Montserrat_SemiBold",
		fontSize: 24,
	},
	prepTime: { fontFamily: "Montserrat_Medium", fontSize: 16 },
	description: { marginTop: 16, fontFamily: "Montserrat" },
	ingredientsHeader: {
		marginTop: 16,
		marginBottom: 8,
		fontFamily: "Montserrat_Medium",
	},
	ingredientWrapper: {
		flexDirection: "row",
		flexWrap: "nowrap",
		alignContent: "center",
		alignItems: "center",
	},
	ingredient: { fontFamily: "Montserrat", marginLeft: 8 },
	stepsHeader: {
		marginTop: 16,
		marginBottom: 8,
		fontFamily: "Montserrat_Medium",
	},
	step: { fontFamily: "Montserrat", marginBottom: 8 },
});
