import React from "react";
import { Text } from "native-base";
import { View } from 'react-native';

const ErrorMessage = props => {
	return (
		<View style={{ backgroundColor: "red", borderRadius: 4, padding: 8 }}>
			<Text style={{ color: "white" }}>
				Could not load word statistics: {props.error.toString()}
			</Text>
		</View>
	);
};
export default ErrorMessage;
