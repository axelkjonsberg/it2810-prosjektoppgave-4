# GooseGooseRun - Recipes app

This project is a React Native port of a [previous React project](https://gitlab.stud.idi.ntnu.no/IT2810-H19/teams/team-25/webutvikling-project-3). The Redux state management system, communication with the backend, and formatting have been transferred and reused to reduce the development time.

## How to run

### Running the published app on Expo (Android only)

0. (If you're not on Eduroam, connect to the NTNU VPN on your phone with the Cisco Anyconnect app)
1. Download and launch the Expo app
1. Scan the QR code, or go [here](https://expo.io/@simpliston/ggr) and scan the QR code on that page. If the link does not work and you're on Android, search for "ggr" or "simpliston" and find the project named GooseGooseRun.

<img src="https://i.imgur.com/fI5i3SS.jpg" alt="QR code"
	title="QR code for published project" width="250" />

### Running the app locally

0. (If you're not on Eduroam, connect to the NTNU VPN on your phone with the Cisco Anyconnect app)
1. Clone the project
1. Install Expo globally if you haven't already: `npm i -g expo-client`
1. Run `expo install` in the project folder (where package.json is)
1. Run `expo start` and go to the localhost address in the console (usually `localhost:19002`)
1. Set "Connection" to "Tunnel"
1. Scan the QR code with your phone using the Expo app

<img src="https://i.imgur.com/ndZ5wvA.jpg" alt="Searching on search screen"
	title="Searching on search screen" width="250" />
<img src="https://i.imgur.com/lTg9Ly0.jpg" alt="Recipe details"
	title="Recipe details" width="250" />
<img src="https://i.imgur.com/4OJxiTy.jpg" alt="Statistics screen"
	title="Statistics screen" width="250" />
<img src="https://i.imgur.com/d0ZYJWW.jpg" alt="Search history"
	title="Search history" width="250" />
<img src="https://i.imgur.com/wSJqiSD.jpg" alt="Search history"
	title="Search history" width="250" />

### Features

- Searching for recipes, with serverside sorting, filters, pagination, and phrase search
- Reading recipe details
- Statistics on most frequent words in recipe names
- Phone-persistent search history
- Adding recipes to Favorites

## Project structure

**|-- .expo-shared** : Hashes of project assets <br>
**|-- assets** : Assets used by the project <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- fonts** : Fonts hosted by the app
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- icon.png** : Icon for the app used when published
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- splash.png** : Splash screen shown on startup
<br>
**|-- constants** : Constants used several places in the project
<br>
**|-- native-base-theme** : Files for customizing nativebase theme
<br>
**|-- navigation** : Files related to app navigation
<br>
**|-- redux** : The store, actions, reducers, selectors and actiontypes Redux needs to work <br>
**|-- screens** : Screens available through navigation. Subscreens and local components are in the top level screen folder <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- SearchScreen** : Folder with searchscreen components and subfolder with recipe details screen
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- FavoritesScreen.js** : Screen shown when user navigates to "Favorites". User picked favorite recipes
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- HistoryScreen.js** : Screen shown when user navigates to "History". Search history
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- StatisticsScreen.js** : Screen shown when user navigates to "Statistics". Stats on most frequent words in recipe names
<br>
**|-- utilities** : General utilites used throughout the project <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- asyncStorageUtils.js** : Functions for using async storage, storing and loading search history and favorites
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- formatUtils.js** : Functions for formatting text throughout the app
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- loadUtils.js** : Functions for loading content from the backend
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- loadUtils.js** : Functions for creating queries and sending them to the backend
<br>
**|-- .eslintrc.json** : Config file for eslint <br>
**|-- .gitignore** <br>
**|-- App.js** : Root component with providers, top level navigator, loading fonts <br>
**|-- app.json** : Expo config <br>
**|-- package-lock.json** <br>
**|-- package.json** <br>
&nbsp;**\\- README.md** <br>
<br>

## Technology

Below is a thorough list of the project's main dependencies and a description of why
they are used.

### react-native

### expo

### react-redux + redux

- Simplifies state management independent of component structure.

### native-base

- UI-library with components capable of changing appearance depending on platform. We chose to use this library because it is highly customizable and has a wide range of components.

### react-navigation

- Declarative, customizable, and well documented app navigation. We chose this module because it is endorsed by the React Native team and is simple to use for creating tab navigation and independent navigation stacks.

### react-native-enhanced-popup-menu

- Simple module for showing popup menus. Was used in order to save time from using modals from React Native, and because it has nice animations.

### react-native-popup-dialog

- Module for displaying popup dialogs. Chosen because it is possible to insert custom content. This allowed us to implement filters and sorting without wasting screen space.

### react-native-slider

- Sliders capable of changing appearance on different platforms. Chosen to save time in implementation and to make it easy for users to pick values in predefined ranges for filters.

### react-native-progress

- Simple module for displaying progress bars. We used this in order to save time in implementing progress bars from scratch.

### eslint + eslint-plugin-react + eslint-plugin-react-native

- Linting and intellisense for the project.

## Reuse of code and porting from earlier project

We've reused most of the code related to communication with the backend, formatting and Redux state management. We used the exact same backend as the last project without modifications. This allowed us to save time, especially during testing stages, since the old and tested code should have a much lower chance of failure.

When starting the port, the first thing we did was to decide which features to keep, which to remove and which to add. We changed the wordcloud to a barchart since it wouldn't be as easy to implement in an app. We cut the user login and authentication functions, since the assigment asked for user data to be stored on the phone instead. We've also added an "favorites" feature in order to complete the user experience. The feature of creating new recipes was cut due to time constraints. After that, we created tabs for the main features and made them one by one. We reused as much code as possible in order to complete the tabs quicker.

In addition, the project design and appearance is heavily inspired by the previous project's design. That way, we did not have to rethink stylings because we could use choices that looked good already. Have a look at a comparison below:

<img src="https://i.imgur.com/ndZ5wvA.jpg" alt="Searching on search screen"
	title="Searching on search screen" width="200" />
<img src="https://i.imgur.com/RjC3jwQ.jpg" alt="Searching on search screen"
	title="Searching on search screen" width="400" />

<img src="https://i.imgur.com/lTg9Ly0.jpg" alt="Recipe details"
	title="Recipe details" width="200" />
<img src="https://i.imgur.com/Xs4OATZ.jpg" alt="Recipe details"
	title="Recipe details" width="400" />

<img src="https://i.imgur.com/4OJxiTy.jpg" alt="Statistics screen"
	title="Statistics screen" width="200" />
<img src="https://i.imgur.com/xdrXOhb.jpg" alt="Statistics screen"
	title="Statistics screen" width="400" />

<img src="https://i.imgur.com/d0ZYJWW.jpg" alt="Search history"
	title="Search history" width="200" />
<img src="https://i.imgur.com/M28N5tH.jpg" alt="Search history"
	title="Search history" width="400" />

## Testing

The application has been thoroughly tested (manually) on selected mobile devices. This is mainly to ensure that elements are displayed in the desired way on both _IOS_ and _Android_ operating systems. We also wanted to ensure that the app's functionality is properly accessible on all mobile devices.
