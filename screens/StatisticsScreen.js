import React, { useEffect, useState } from "react";
import { StyleSheet, View, ScrollView, Dimensions } from "react-native";

import Colors, { materialColors } from "../constants/Colors";
import { Container, Spinner, Content, H1, Text, Button } from "native-base";
import { connect } from "react-redux";
import { setSearchValue, resetPage, setLoadModeLoad } from "../redux/actions";
import ErrorMessage from "../components/ErrorMessage";

const WordcloudScreen = props => {
	const [words, setWords] = useState([]);
	const [highestCount, setHighestCount] = useState();

	const deviceWidth = Dimensions.get("window").width;

	useEffect(() => {
		if (props.wordFrequency) {
			const formattedWords = props.wordFrequency.map((word, index) => {
				const wordColor = materialColors[index % materialColors.length];
				return {
					word: word.word,
					count: word.count,
					color: wordColor,
				};
			});
			setHighestCount(formattedWords[0].count);
			setWords(formattedWords);
		}
	}, [props.wordFrequency]);

	function handleWordPress(word) {
		const stringWithoutBackslash = word.replace(/\\/g, "");
		const stringWithEscapedQuotes = stringWithoutBackslash.replace(/"/g, '\\"');
		props.dispatch(setSearchValue(stringWithEscapedQuotes));
		props.dispatch(setLoadModeLoad());
		props.dispatch(resetPage());
		props.navigation.navigate("Search");
	}

	if (props.loading) {
		return (
			<Container style={{ justifyContent: "center", alignContent: "center" }}>
				<Spinner color={Colors.primary} />
			</Container>
		);
	} else if (props.failure) {
		return (
			<Container>
				<Content padder>
					<ErrorMessage error={props.error} />
				</Content>
			</Container>
		);
	}

	const renderWord = (word, index) => {
		const minWidth = 100;
		const maxWidth = deviceWidth - 80 - minWidth;
		const frequencyRatio = word.count / highestCount;
		const width = maxWidth * frequencyRatio + minWidth;

		return (
			<View style={styles.item} transparent key={index}>
				<View style={styles.data}>
					<View style={{ ...styles.barWrapper, width: width }}>
						<Button
							style={{
								...styles.bar,
								backgroundColor: word.color,
							}}
							full
							onPress={() => handleWordPress(word.word)}
						>
							<Text style={styles.label}>{word.word}</Text>
						</Button>
					</View>
					<Text style={styles.wordCount}>{word.count}</Text>
				</View>
			</View>
		);
	};

	return (
		<Container>
			<Content padder>
				<H1 style={styles.title}>Most frequent words in recipe names</H1>
				{words
					? words.map((word, index) => renderWord(word, index))
					: undefined}
				<View style={styles.bottomFiller}></View>
			</Content>
		</Container>
	);
};

const mapStateToProps = state => ({
	loading: state.wordFrequency.loading,
	failure: state.wordFrequency.failure,
	error: state.wordFrequency.error,
	wordFrequency: state.wordFrequency.wordFrequency,
});

export default connect(mapStateToProps)(WordcloudScreen);

const styles = StyleSheet.create({
	content: {
		flexDirection: "column",
	},
	wordcloud: {
		width: "100%",
		height: "90%",
	},
	title: {
		margin: 18,
		fontFamily: "Montserrat_SemiBold",
	},
	barWrapper: {},
	bar: {
		justifyContent: "flex-start",
		alignContent: "center",
		borderRadius: 24,
		height: 44,
		marginRight: 5,
	},
	item: {
		flexDirection: "column",
		marginBottom: 10,
	},
	label: {
		color: "#F5F5F5",
		textTransform: "lowercase",
		fontWeight: "400",
		fontSize: 16,
		marginLeft: 12,
	},
	data: {
		flex: 2,
		flexDirection: "row",
		maxWidth: "100%",
		alignItems: "center",
	},
	wordCount: {
		fontSize: 14,
		color: "#424242",
	},
	bottomFiller: {
		height: 60,
	},
});
