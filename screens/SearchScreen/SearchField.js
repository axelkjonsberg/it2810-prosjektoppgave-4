import React, { useRef } from "react";
import { Item, Icon, Input, Button } from "native-base";
import { View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import Menu, { MenuItem, Position } from "react-native-enhanced-popup-menu";

import {
	setSearchValue,
	resetPage,
	setLoadModeLoad,
} from "../../redux/actions";
import Colors from "../../constants/Colors";
import { formatSearchValue } from "../../utilities/formatUtils";

const SearchField = props => {
	let menuOriginRef = useRef();
	let menuRef = useRef();

	const hideMenu = () => menuRef.current.hide();
	const showMenu = () => {
		menuRef.current.show(menuOriginRef.current, (stickTo = Position.TOP_LEFT));
	};

	const openSortMenu = () => {
		hideMenu();
		props.setSortMenuOpen(true);
	};

	const openFilterMenu = () => {
		hideMenu();
		props.setFilterMenuOpen(true);
	};

	const handleSubmitSearch = e => {
		const stringWithoutBackslash = e.nativeEvent.text.replace(/\\/g, "");
		const stringWithEscapedQuotes = stringWithoutBackslash.replace(/"/g, '\\"');
		props.dispatch(setSearchValue(stringWithEscapedQuotes));
		props.dispatch(setLoadModeLoad());
		props.dispatch(resetPage());
	};

	return (
		<View
			style={{
				marginVertical: 16,
				width: "95%",
			}}
		>
			<Item rounded>
				<Icon active name='search' />
				<Input
					keyboardType='web-search'
					defaultValue={formatSearchValue(props.searchValue)}
					placeholder='Search for recipes...'
					onSubmitEditing={e => handleSubmitSearch(e)}
					selectionColor={Colors.primary}
				/>
				<View
					ref={menuOriginRef}
					style={{ borderRadius: 44, overflow: "hidden" }}
				>
					<Button transparent onPress={() => showMenu()}>
						<View
							style={{
								height: 44,
								width: 44,
								display: "flex",
								justifyContent: "center",
								alignContent: "center",
								alignItems: "center",
							}}
						>
							<Icon name='more' style={{ color: "#bbb", fontSize: 27 }} />
						</View>
					</Button>
				</View>
			</Item>
			<Menu ref={menuRef}>
				<MenuItem textStyle={styles.menuItem} onPress={openSortMenu}>
					Sort
				</MenuItem>
				<MenuItem textStyle={styles.menuItem} onPress={openFilterMenu}>
					Filter
				</MenuItem>
			</Menu>
		</View>
	);
};

const mapStateToProps = state => ({
	searchValue: state.recipeSearch.searchValue,
});

export default connect(mapStateToProps)(SearchField);

const styles = StyleSheet.create({
	menuItem: { fontSize: 17 },
});
