import React, { useState, useRef } from "react";
import { connect } from "react-redux";
import { StyleSheet, View } from "react-native";
import { Container } from "native-base";
import * as Progress from "react-native-progress";

import SearchField from "./SearchField";
import SearchSort from "./SearchSort";
import SearchFilter from "./SearchFilter";
import SearchResults from "./SearchResults";
import Colors from "../../constants/Colors";

const SearchScreen = props => {
	const [sortMenuOpen, setSortMenuOpen] = useState(false);
	const [filterMenuOpen, setFilterMenuOpen] = useState(false);

	return (
		<Container>
			<View
				style={{
					justifyContent: "center",
					flexDirection: "column",
					flexWrap: "nowrap",
					flexGrow: 1,
					flexShrink: 1,
				}}
			>
				<Progress.Bar
					indeterminate
					color={
						props.loading && props.navigation.isFocused()
							? Colors.primary
							: "white"
					}
					width={null}
					borderWidth={0}
					borderRadius={0}
					animationType='spring'
					animationConfig={{ bounciness: 100 }}
				/>
				<SearchSort
					sortField={props.sortField}
					sortDESC={props.sortDESC}
					dispatch={props.dispatch}
					open={sortMenuOpen}
					close={() => setSortMenuOpen(false)}
				/>
				<SearchFilter
					filters={props.filters}
					dispatch={props.dispatch}
					open={filterMenuOpen}
					close={() => setFilterMenuOpen(false)}
				/>
				<View style={{ alignItems: "center" }}>
					<SearchField
						setSortMenuOpen={setSortMenuOpen}
						setFilterMenuOpen={setFilterMenuOpen}
					/>
				</View>
				<SearchResults />
			</View>
		</Container>
	);
};

const mapStateToProps = state => ({
	sortField: state.recipeSort.sortField,
	sortDESC: state.recipeSort.sortDESC,
	filters: state.recipeFilters,
	loading: state.loading.loading,
});

export default connect(mapStateToProps)(SearchScreen);
