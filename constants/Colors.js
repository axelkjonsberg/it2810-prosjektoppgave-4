const primary = "#3949AB";

export default {
	primary,
	tabIconDefault: "#ccc",
	tabIconSelected: primary,
	tabBar: "#fefefe",
	errorBackground: "red",
	errorText: "#fff",
	warningBackground: "#EAEB5E",
	warningText: "#666804",
	noticeBackground: primary,
	noticeText: "#fff",
};

export const materialColors = [
	"#E53935",
	"#D81B60",
	"#8E24AA",
	"#5E35B1",
	"#3949AB",
	"#1E88E5",
	"#0288D1",
	"#0097A7",
	"#00897B",
	"#43A047",
	"#558B2F",
	"#827717",
	"#E65100",
];
